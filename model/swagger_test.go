package model_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/proto-swagger-pjrpc/model"
)

func TestSchemaReference(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	schema := &model.Schema{Name: "NameOfSchema"}
	ref := schema.Reference()

	so.Equal("#/definitions/NameOfSchema", ref.Ref)
	so.Equal("", ref.Name)
}
