.PHONY: test coverage lints lints_fix build build_to_bin proto

test:
	go test -cover ./...

coverage:
	go test -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -func profile.cov

	# last line will be number of total coverage
	go tool cover -func profile.cov | tail -n1 | awk '{print $$3}'

lints:
	golangci-lint version
	golangci-lint run -c ./.linters.yml

lints_fix:
	golangci-lint run --fix -c ./.linters.yml

build:
	go build -o ./cmd/protoc-gen-swagger-pjrpc/protoc-gen-swagger-pjrpc ./cmd/protoc-gen-swagger-pjrpc

build_to_bin: build
	cp ./cmd/protoc-gen-swagger-pjrpc/protoc-gen-swagger-pjrpc $(GOBIN)/protoc-gen-swagger-pjrpc

proto:
	cd ./genswagger/ && protoc \
		-I ./testdata/proto/ \
		--swagger-pjrpc_out=service_descriptions=./testdata/proto/service_descriptions.json,path_proto_request=./testdata/proto/proto_request_all.txt:./testdata/swagger/all/ \
		./testdata/proto/test_proto.proto

	cd ./genswagger/ && protoc \
		-I ./testdata/proto/ \
		--swagger-pjrpc_out=services=WithoutComments,services_version=v1.0.10,path_proto_request=./testdata/proto/proto_request_single.txt:./testdata/swagger/single/ \
		./testdata/proto/test_proto.proto
	
	cd ./genswagger/ && protoc \
		-I ./testdata/proto/ \
		--swagger-pjrpc_out=services=WithComments,all_types=true,hide_versions=true,path_proto_request=./testdata/proto/proto_request_all_types.txt:./testdata/swagger/all_types/ \
		./testdata/proto/test_proto.proto
