package model

// ServiceDescriptions is a map "ServiceName" -> *Swagger spec.
type ServiceDescriptions map[string]*Swagger
