module gitlab.com/pjrpc/proto-swagger-pjrpc

go 1.15

require (
	github.com/stretchr/testify v1.6.1
	gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjrpc v1.0.0
	gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjson v1.0.0
	google.golang.org/protobuf v1.25.0
)
