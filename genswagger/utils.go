package genswagger

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/reflect/protoreflect"

	"gitlab.com/pjrpc/proto-swagger-pjrpc/model"
)

func loadServiceDescriptions(path string) (model.ServiceDescriptions, error) {
	if path == "" {
		return make(model.ServiceDescriptions), nil
	}

	file, err := os.Open(path) // nolint:gosec // open by path from config
	if err != nil {
		return nil, fmt.Errorf("open file with ServiceDescriptions: %w", err)
	}

	descriptions := make(model.ServiceDescriptions)
	err = json.NewDecoder(file).Decode(&descriptions)
	if err != nil {
		return nil, fmt.Errorf("decode file with ServiceDescriptions: %w", err)
	}

	return descriptions, nil
}

func setStringIfEmpty(target *string, value string) {
	if *target == "" {
		*target = value
	}
}

func addText(left, right string) string {
	if left == "" {
		return right
	}

	if right == "" {
		return left
	}

	if strings.HasSuffix(left, "\n") || strings.HasPrefix(right, "\n") {
		return left + right
	}

	return left + " " + right
}

const swaggerCommentTag = "swagger-pjrpc_out_tags"

type jsonComment protogen.CommentSet

func (c jsonComment) String() string {
	lines := make([]string, 0)
	for _, line := range strings.Split(strings.ReplaceAll(c.Leading.String(), "\r\n", "\n"), "\n") {
		if strings.Contains(line, swaggerCommentTag) {
			continue
		}

		lines = append(lines, strings.TrimSpace(strings.ReplaceAll(line, "//", "")))
	}
	leading := strings.TrimSpace(strings.Join(lines, " "))

	lines = lines[:0]
	for _, line := range strings.Split(strings.ReplaceAll(c.Trailing.String(), "\r\n", "\n"), "\n") {
		lines = append(lines, strings.TrimSpace(strings.ReplaceAll(line, "//", "")))
	}
	trailing := strings.TrimSpace(strings.Join(lines, " "))

	return addText(leading, trailing)
}

func getLeadingComments(comments protogen.CommentSet) []string {
	leading := strings.ReplaceAll(string(comments.Leading), "//", "")
	return strings.Split(strings.ReplaceAll(leading, "\r\n", "\n"), "\n")
}

func isDeprecated(options protoreflect.ProtoMessage) bool {
	type deprecatedGetter interface {
		GetDeprecated() bool
	}

	deprecated := false
	if v, ok := options.(deprecatedGetter); ok {
		deprecated = v.GetDeprecated()
	}

	return deprecated
}

func addDeprecated(options protoreflect.ProtoMessage, comments protogen.CommentSet) string {
	deprecatedText := ""
	if isDeprecated(options) {
		deprecatedText = "Deprecated: Do not use."
	}

	return addText(deprecatedText, jsonComment(comments).String())
}

func uniqueStrings(list []string) []string {
	uniqueControl := make(map[string]struct{})
	n := 0

	for _, val := range list {
		_, ok := uniqueControl[val]
		if ok {
			continue
		}

		uniqueControl[val] = struct{}{}
		list[n] = val
		n++
	}

	list = list[:n]

	return list
}
