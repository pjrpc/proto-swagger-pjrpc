package genswagger

import (
	"errors"
)

var (
	// ErrNoServicesInFiles is returned when all proto-files do not contain service directives.
	ErrNoServicesInFiles = errors.New("proto files do not contain services")
	// ErrServiceNotFound is returned when service with required name not found in list service.
	ErrServiceNotFound = errors.New("service not found")
	// ErrInvalidKeyType is returned when key of the map field is wrong.
	ErrInvalidKeyType = errors.New("key of the map should be string or integer")
)
