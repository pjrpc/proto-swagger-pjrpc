// Package flag contains variables and flags set for generator.
package flag

import (
	"flag"
)

var (
	// Version of the generator.
	Version = "v0.0.5"

	// Flags set with config flags.
	Flags flag.FlagSet

	// PathProtoRequest path to save protobuf request, will not save the file if the value is empty.
	PathProtoRequest = Flags.String(
		"path_proto_request",
		"",
		"Path to save protobuf request, will not save the file if the value is empty.",
	)

	// MethodsWithServiceName JSON-RPC method name will be with service name prefix 'service_name_method_name'.
	MethodsWithServiceName = Flags.Bool(
		"methods_with_service_name",
		false,
		"Method name will be with service name prefix 'service_name_method_name'.",
	)

	// Services is service names for generation docs. All services if the flag is empty. (service;srv2;srv3).
	Services = Flags.String(
		"services",
		"",
		"Service names for generation docs. All services if the flag is empty. (service;srv2;srv3).",
	)

	// ServiceDescriptions  is a path to JSON file with swagger service descriptions.
	ServiceDescriptions = Flags.String(
		"service_descriptions",
		"",
		"Path to JSON file with swagger service descriptions.",
	)

	// ServicesVersion allow to set version of the API by generator flag.
	ServicesVersion = Flags.String(
		"services_version",
		"",
		"Version of the services. Replaced version value from service_descriptions.",
	)

	// AllTypes genereates all type from proto files.
	AllTypes = Flags.Bool(
		"all_types",
		false,
		"Generates all types from proto files. By default, only used types.",
	)

	// HideVersions does not print versions of the bins.
	HideVersions = Flags.Bool(
		"hide_versions",
		false,
		"Does not print version of the protoc and protoc-gen-swagger-pjrpc",
	)
)
