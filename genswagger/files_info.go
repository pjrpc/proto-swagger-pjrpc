package genswagger

import (
	"gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjrpc/genpjrpc"
	"gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjson/genpjson"
	"google.golang.org/protobuf/compiler/protogen"
)

type serviceInfo struct {
	*genpjrpc.ServiceInfo

	file *genpjson.FileInfo
}

type filesInfo struct {
	files    []*genpjson.FileInfo
	services map[string]*serviceInfo

	allMessages map[string]*genpjson.MessageInfo
	allEnums    map[string]*genpjson.EnumInfo
}

func parseFiles(gen *protogen.Plugin, files []*protogen.File) (allFiles []*genpjson.FileInfo, services map[string]*serviceInfo) {
	allFiles = make([]*genpjson.FileInfo, 0, len(files))
	services = make(map[string]*serviceInfo, len(files))

	for _, f := range files {
		if !f.Generate {
			continue
		}

		fileInfo := genpjson.GetFileInfo(f)
		allFiles = append(allFiles, fileInfo)

		for _, service := range f.Services {
			sInfo := &serviceInfo{
				ServiceInfo: genpjrpc.GetServiceInfo(service),
				file:        fileInfo,
			}
			services[string(service.Desc.Name())] = sInfo
		}

		imps := f.Desc.Imports()
		for i := 0; i < imps.Len(); i++ {
			imp := imps.Get(i)
			impFile := gen.FilesByPath[imp.Path()]
			allFiles = append(allFiles, genpjson.GetFileInfo(impFile))
		}
	}

	return allFiles, services
}

func getFilesInfo(gen *protogen.Plugin, files []*protogen.File) *filesInfo {
	allFiles, services := parseFiles(gen, files)

	info := &filesInfo{
		files:    allFiles,
		services: services,

		allMessages: make(map[string]*genpjson.MessageInfo, 100),
		allEnums:    make(map[string]*genpjson.EnumInfo, 10),
	}

	for _, f := range info.files {
		for _, msg := range f.Messages {
			info.allMessages[msg.GoIdent.String()] = msg
		}

		for _, enum := range f.Enums {
			info.allEnums[enum.GoIdent.String()] = enum
		}
	}

	return info
}
