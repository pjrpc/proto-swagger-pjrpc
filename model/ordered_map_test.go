package model_test

import (
	"encoding/json"
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/proto-swagger-pjrpc/model"
)

func TestOrderedMap_MarshalJSON(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	om := make(model.OrderedMap, 0, 5)

	om = om.Add("Zkey", 10).
		Add("aKey", 5).
		Add("0123", 3).
		Add("3210", 2).
		Add("qwerty", 0)

	type object struct {
		Field      string           `json:"field"`
		OrderedMap model.OrderedMap `json:"ordered_map"`
	}

	j := &object{
		Field:      "text",
		OrderedMap: om,
	}

	res, err := json.Marshal(j)
	so.NoError(err)
	so.Equal(`{"field":"text","ordered_map":{"Zkey":10,"aKey":5,"0123":3,"3210":2,"qwerty":0}}`, string(res))

	om = om.Add("invalid_value", make(chan int))
	j.OrderedMap = om
	_, err = json.Marshal(j)

	jErr := new(json.UnsupportedTypeError)
	so.Truef(errors.As(err, &jErr), "wrong error: %#v", err)
}
