Proto-Swagger-PJRPC
========

[Swagger spec](https://swagger.io/specification/v2/) generator using protobuf files.

The generator uses some of the rules of [pjrpc](https://gitlab.com/pjrpc/pjrpc) generators such as:
- Go Service naming
- snake_case methods naming

# Installation and use

```sh
go get -u gitlab.com/pjrpc/proto-swagger-pjrpc/cmd/protoc-gen-swagger-pjrpc
```

```sh
protoc --swagger-pjrpc_out=. ./protodir/file.proto
```

Allows to use import proto packages and multiple services per file.

- If you generate one service then you will get one file named `swagger.json`.
- Generating multiple services will create a swagger file for each service `swagger_ServiceName.json`.

## Flags

- `services` (string) - Service names for generation docs. All services if the flag is empty. Example: "service;srv2;srv3".
- `service_descriptions` (string) - Path to JSON file with swagger service descriptions.
- `services_version` (string) - Version of the services. Replaced version value from service_descriptions.
- `all_types` (bool) - Generates all types from proto files. By default, only used types.
- `hide_versions` (bool) - Does not print version of the protoc and protoc-gen-swagger-pjrpc.
- `methods_with_service_name` (bool) - JSON-RPC method name will be with service name prefix 'service_name_method_name'.


### Examples:

**With swagger info file:**

```sh
protoc \
	-I ./proto/ \
	--swagger-pjrpc_out=service_descriptions=./proto/service_descriptions.json:./swagger/ \
	./proto/file.proto
```

**With one service, service version and all types of files**

```sh
protoc \
	-I ./proto/ \
	--swagger-pjrpc_out=services=ServiceName,services_version=v1.0.10,all_types=true:./swagger/ \
	./proto/file.proto
```

# Swagger Service info

You can add swagger information of the service to the Swagger spec. See flag `service_descriptions`.

Without file will be generate default info values and descriptions based on comments.

The file can contain all fields of the [swagger spec](https://swagger.io/specification/v2/) except:
- consumes
- produces
- paths
- definitions

Fields of the Info (title, description, version) will be replaced if empty only.

File example:
```
{
  "SeviceName": {
    "info": {
      "title": "Service of the ...",
      "version": "v1.1.0"
    }
  },
  "AnotherServiceName": {
    "info": {
      "title": "Another",
      "description": "Description of the Service",
      "version": "v1.3.0"
    }
  },
  "SecureService": {
    "host": "127.0.0.1:8080",
    "basePath": "/rpc",
    "schemes": [
      "http"
    ],
    "securityDefinitions": {
      "api_key": {
        "type": "apiKey",
        "name": "api_key",
        "in": "header"
      }
    },
    "security": [
      {
        "api_key": []
      }
    ]
  }
}
```

# Method tags

You can add your own tags to each methods using protobuf rpc method comments.

Magic comment in line `// swagger-pjrpc_out_tags:` and list of the tags with `,` as separator.

For example:
```
  // MethodWithCustomTag method with custom swagger tag.
  // swagger-pjrpc_out_tags:custom tag,second_tag
  rpc MethodWithCustomTag (AllFields) returns (DeprecatedMsg) {};
```
