package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/encoding/prototext"

	"gitlab.com/pjrpc/proto-swagger-pjrpc/cmd/protoc-gen-swagger-pjrpc/flag"
	"gitlab.com/pjrpc/proto-swagger-pjrpc/genswagger"
)

func run(gen *protogen.Plugin) error {
	genswagger.Version = flag.Version
	genswagger.MethodsWithServiceName = *flag.MethodsWithServiceName
	genswagger.ServiceDescriptionsPath = *flag.ServiceDescriptions
	genswagger.ServicesVersion = *flag.ServicesVersion
	genswagger.AllTypes = *flag.AllTypes
	genswagger.HideVersions = *flag.HideVersions

	serviceNames := strings.Split(*flag.Services, ";")
	if serviceNames[0] != "" {
		genswagger.GenerateServices = serviceNames
	}

	err := genswagger.Run(gen)
	if err != nil {
		return fmt.Errorf("run generator: %w", err)
	}

	if *flag.PathProtoRequest == "" {
		return nil
	}

	file, err := os.Create(*flag.PathProtoRequest)
	if err != nil {
		return fmt.Errorf("failed to create file for proto request: %w", err)
	}

	request, err := prototext.Marshal(gen.Request)
	if err != nil {
		return fmt.Errorf("prototext.Marshal: %w", err)
	}

	if _, err = file.Write(request); err != nil {
		return fmt.Errorf("file.Write: %w", err)
	}

	if err = file.Close(); err != nil {
		return fmt.Errorf("file.Close: %w", err)
	}

	return nil
}

func showVesrionsOrHelp() {
	if os.Args[1] == "--version" || os.Args[1] == "version" {
		fmt.Fprintf(os.Stderr, "%s %s\n", filepath.Base(os.Args[0]), flag.Version)
		return
	}

	flag.Flags.PrintDefaults()
}

func main() {
	if len(os.Args) == 2 { // nolint:gomnd // bin_name --version
		showVesrionsOrHelp()
		os.Exit(0)
	}

	o := protogen.Options{
		ParamFunc:         flag.Flags.Set,
		ImportRewriteFunc: nil,
	}

	o.Run(run)
}
