package genswagger

// Some variables as config of generator.
var (
	// Version of the generator.
	Version = "v0.0.0-unknown"
	// MethodsWithServiceName specifies that name of methods will be generate with service name.
	MethodsWithServiceName = false
	// GenerateServices service names to generate, all if it empty.
	GenerateServices = []string{}
	// ServiceDescriptionsPath path to json file with service descriptions.
	ServiceDescriptionsPath = ""
	// ServicesVersion sets the version of the services if it is not empty.
	ServicesVersion = ""
	// AllTypes genereates all type from proto files.
	AllTypes = false
	// HideVersions do not prints version of the protoc and protoc-gen-swagger-pjrpc.
	HideVersions = false

	swaggerVersion = "2.0"
)
