package model

import (
	"bytes"
	"encoding/json"
	"fmt"
)

// OrderedKeyVal key-value struct keep order in map.
type OrderedKeyVal struct {
	Key   string
	Value interface{}
}

// OrderedMap allows to keep order fields of the object.
// {"key":{"some":"value"}, "key2":{}}.
type OrderedMap []*OrderedKeyVal

// Add adds to ordered map key with value.
func (om OrderedMap) Add(key string, value interface{}) OrderedMap {
	om = append(om, &OrderedKeyVal{
		Key:   key,
		Value: value,
	})

	return om
}

// MarshalJSON implements JSON marshaler interface.
func (om OrderedMap) MarshalJSON() ([]byte, error) {
	var buf bytes.Buffer

	buf.WriteString("{")

	for i, kv := range om {
		if i != 0 {
			buf.WriteString(",")
		}

		key, _ := json.Marshal(kv.Key) // nolint:errcheck // impossible error of string type.

		val, err := json.Marshal(kv.Value)
		if err != nil {
			return nil, fmt.Errorf("json.Marshal val: %w", err)
		}

		buf.Write(key)
		buf.WriteString(":")
		buf.Write(val)
	}

	buf.WriteString("}")

	return buf.Bytes(), nil
}
