package genswagger_test

import (
	"io/ioutil"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/encoding/prototext"
	"google.golang.org/protobuf/types/pluginpb"

	"gitlab.com/pjrpc/proto-swagger-pjrpc/cmd/protoc-gen-swagger-pjrpc/flag"
	"gitlab.com/pjrpc/proto-swagger-pjrpc/genswagger"
)

func getExpectedFiles(so *require.Assertions, paths []string) map[string]string {
	res := make(map[string]string, len(paths))

	for _, path := range paths {
		fileJSON, err := ioutil.ReadFile(path) // nolint:gosec // read files from local var
		so.NoError(err)

		res[filepath.Base(path)] = string(fileJSON)
	}

	return res
}

func resetFlags(so *require.Assertions) {
	so.NoError(flag.Flags.Set("path_proto_request", ""))
	so.NoError(flag.Flags.Set("methods_with_service_name", "false"))
	so.NoError(flag.Flags.Set("services", ""))
	so.NoError(flag.Flags.Set("service_descriptions", ""))
	so.NoError(flag.Flags.Set("services_version", ""))
	so.NoError(flag.Flags.Set("all_types", "false"))
	so.NoError(flag.Flags.Set("hide_versions", "false"))
}

func setFlags() {
	genswagger.Version = flag.Version
	genswagger.MethodsWithServiceName = *flag.MethodsWithServiceName
	genswagger.ServiceDescriptionsPath = *flag.ServiceDescriptions
	genswagger.ServicesVersion = *flag.ServicesVersion
	genswagger.AllTypes = *flag.AllTypes
	genswagger.HideVersions = *flag.HideVersions

	serviceNames := strings.Split(*flag.Services, ";")
	if serviceNames[0] != "" {
		genswagger.GenerateServices = serviceNames
	}
}

var (
	requestPath      = "./testdata/proto/"
	expectedFilePath = "./testdata/swagger/"
)

func testRequest(t *testing.T, requestPath string, expectedFiles ...string) { // nolint:thelper // this is not helper
	t.Log("requestPath:   ", requestPath)
	t.Log("expectedFiles: ", expectedFiles)

	so := require.New(t)
	resetFlags(so)

	expFiles := getExpectedFiles(so, expectedFiles)

	requestDump, err := ioutil.ReadFile(requestPath) // nolint:gosec // read files from local var
	if err != nil {
		t.Fatal("ioutil.ReadFile dump file: ", err)
	}

	request := new(pluginpb.CodeGeneratorRequest)
	err = prototext.Unmarshal(requestDump, request)
	so.NoError(err)

	o := protogen.Options{
		ParamFunc:         flag.Flags.Set,
		ImportRewriteFunc: nil,
	}

	plugin, err := o.New(request)
	so.NoError(err)

	setFlags()

	err = genswagger.Run(plugin)
	so.NoError(err)

	resp := plugin.Response()

	so.Equal(len(expFiles), len(resp.File))

	for _, file := range resp.GetFile() {
		expectedFile, ok := expFiles[filepath.Base(file.GetName())]
		so.Truef(ok, "expectedFile not found '%s'", file.GetName())

		so.JSONEq(expectedFile, file.GetContent(), file.GetName())
	}
}

func TestRun(t *testing.T) {
	t.Parallel()

	testRequest(
		t,
		requestPath+"proto_request_all.txt",
		expectedFilePath+"all/swagger_Deprecated.json",
		expectedFilePath+"all/swagger_DeprecatedWithComments.json",
		expectedFilePath+"all/swagger_WithComments.json",
		expectedFilePath+"all/swagger_WithoutComments.json",
	)

	testRequest(
		t,
		requestPath+"proto_request_single.txt",
		expectedFilePath+"single/swagger.json",
	)

	testRequest(
		t,
		requestPath+"proto_request_all_types.txt",
		expectedFilePath+"all_types/swagger.json",
	)
}
