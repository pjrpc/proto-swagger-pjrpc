// Package model contains swagger and generator models.
package model

import (
	"fmt"
)

// Swagger is the root document object for the API specification.
// It combines what previously was the Resource Listing and API Declaration
// (version 1.2 and earlier) together into one document.
type Swagger struct {
	// Required. Specifies the Swagger Specification version being used.
	// It can be used by the Swagger UI and other clients to interpret the API listing.
	// The value MUST be "2.0".
	Swagger string `json:"swagger"`
	// Required. Provides metadata about the API. The metadata can be used by the clients if needed.
	Info *Info `json:"info"`
	// The host (name or ip) serving the API.
	// This MUST be the host only and does not include the scheme nor sub-paths.
	// It MAY include a port. If the host is not included, the host serving the documentation is
	// to be used (including the port). The host does not support path templating.
	Host string `json:"host,omitempty"`
	// The base path on which the API is served, which is relative to the host.
	// If it is not included, the API is served directly under the host.
	// The value MUST start with a leading slash (/). The basePath does not support path templating.
	BasePath string `json:"basePath,omitempty"`
	// The transfer protocol of the API. Values MUST be from the list: "http", "https", "ws", "wss".
	// If the schemes is not included, the default scheme to be used is the one used to access the
	// Swagger definition itself.
	Schemes []string `json:"schemes,omitempty"`
	// A list of MIME types the APIs can consume.
	// This is global to all APIs but can be overridden on specific API calls.
	// Value MUST be as described under Mime Types.
	Consumes []string `json:"consumes,omitempty"`
	// A list of MIME types the APIs can produce.
	// This is global to all APIs but can be overridden on specific API calls.
	// Value MUST be as described under Mime Types.
	Produces []string `json:"produces,omitempty"`
	// Required. The available paths and operations for the API.
	// A relative path to an individual endpoint.
	// The field name MUST begin with a slash.
	// The path is appended to the basePath in order to construct the full URL. Path templating is allowed.
	// "/path" -> *PathItem.
	Paths OrderedMap `json:"paths"`
	// An object to hold data types produced and consumed by operations.
	// "SchemaName" -> *Schema.
	Definitions map[string]*Schema `json:"definitions,omitempty"`
	// An object to hold parameters that can be used across operations.
	// This property does not define global parameters for all operations.
	// "parameterName" -> *Parameter.
	Parameters map[string]*Parameter `json:"parameters,omitempty"`
	// An object to hold responses that can be used across operations.
	// This property does not define global responses for all operations.
	// "responseName" -> *Response.
	Responses map[string]*Response `json:"responses,omitempty"`
	// Security scheme definitions that can be used across the specification.
	SecurityDefinitions map[string]*Security `json:"securityDefinitions,omitempty"`
	// A declaration of which security schemes are applied for the API as a whole.
	// The list of values describes alternative security schemes that can be used
	// (that is, there is a logical OR between the security requirements).
	// Individual operations can override this definition.
	Security []map[string][]string `json:"security,omitempty"`
	// A list of tags used by the specification with additional metadata.
	// The order of the tags can be used to reflect on their order by the parsing tools.
	// Not all tags that are used by the Operation Object must be declared.
	// The tags that are not declared may be organized randomly or based on the tools' logic.
	// Each tag name in the list MUST be unique.
	Tags []*Tag `json:"tags,omitempty"`
	// Additional external documentation.
	ExternalDocs *ExternalDocumentation `json:"externalDocs,omitempty"`
}

// Info provides metadata about the API.
// The metadata can be used by the clients if needed,
// and can be presented in the Swagger-UI for convenience.
type Info struct {
	// Required. The title of the application.
	Title string `json:"title"`
	// A short description of the application. GFM syntax can be used for rich text representation.
	Description string `json:"description,omitempty"`
	// The Terms of Service for the API.
	TermsOfService string `json:"termsOfService,omitempty"`
	// The contact information for the exposed API.
	Contact *Contact `json:"contact,omitempty"`
	// The license information for the exposed API.
	License *License `json:"license,omitempty"`
	// Required Provides the version of the application API
	// (not to be confused with the specification version).
	Version string `json:"version"`
}

// Contact information for the exposed API.
type Contact struct {
	// The identifying name of the contact person/organization.
	Name string `json:"name,omitempty"`
	// The URL pointing to the contact information. MUST be in the format of a URL.
	URL string `json:"url,omitempty"`
	// The email address of the contact person/organization. MUST be in the format of an email address.
	Email string `json:"email,omitempty"`
}

// License information for the exposed API.
type License struct {
	// Required. The license name used for the API.
	Name string `json:"name"`
	// A URL to the license used for the API. MUST be in the format of a URL.
	URL string `json:"url,omitempty"`
}

// PathItem describes the operations available on a single path.
// A Path Item may be empty, due to ACL constraints.
// The path itself is still exposed to the documentation viewer but they
// will not know which operations and parameters are available.
type PathItem struct {
	// Allows for an external definition of this path item.
	// The referenced structure MUST be in the format of a Path Item Object.
	// If there are conflicts between the referenced definition and this Path Item's definition,
	// the behavior is undefined.
	Ref string `json:"$ref,omitempty"`

	// A definition of a GET operation on this path.
	Get *Operation `json:"get,omitempty"`
	// A definition of a PUT operation on this path.
	Put *Operation `json:"put,omitempty"`
	// A definition of a POST operation on this path.
	Post *Operation `json:"post,omitempty"`
	// A definition of a DELETE operation on this path.
	Delete *Operation `json:"delete,omitempty"`
	// A definition of a OPTIONS operation on this path.
	Options *Operation `json:"options,omitempty"`
	// A definition of a HEAD operation on this path.
	Head *Operation `json:"head,omitempty"`
	// A definition of a PATCH operation on this path.
	Patch *Operation `json:"patch,omitempty"`

	// A list of parameters that are applicable for all the operations described under this path.
	// These parameters can be overridden at the operation level, but cannot be removed there.
	// The list MUST NOT include duplicated parameters.
	// A unique parameter is defined by a combination of a name and location.
	// The list can use the Reference Object to link to parameters that are defined
	// at the Swagger Object's parameters. There can be one "body" parameter at most
	Parameters []*Parameter `json:"parameters,omitempty"`
}

// Operation describes a single API operation on a path.
type Operation struct {
	// A list of tags for API documentation control.
	// Tags can be used for logical grouping of operations by resources or any other qualifier.
	Tags []string `json:"tags,omitempty"`
	// A short summary of what the operation does. For maximum readability in the swagger-ui,
	// this field SHOULD be less than 120 characters.
	Summary string `json:"summary,omitempty"`
	// A verbose explanation of the operation behavior. GFM syntax can be used for rich text representation.
	Description string `json:"description,omitempty"`
	// Additional external documentation for this operation.
	ExternalDocs *ExternalDocumentation `json:"externalDocs,omitempty"`
	// Unique string used to identify the operation.
	// The id MUST be unique among all operations described in the API.
	// Tools and libraries MAY use the operationId to uniquely identify an operation,
	// therefore, it is recommended to follow common programming naming conventions.
	OperationID string `json:"operationId,omitempty"`
	// A list of MIME types the operation can consume.
	// This overrides the consumes definition at the Swagger Object.
	// An empty value MAY be used to clear the global definition.
	// Value MUST be as described under Mime Types.
	Consumes []string `json:"consumes,omitempty"`
	// A list of MIME types the operation can produce.
	// This overrides the produces definition at the Swagger Object.
	// An empty value MAY be used to clear the global definition.
	// Value MUST be as described under Mime Types.
	Produces []string `json:"produces,omitempty"`
	// A list of parameters that are applicable for this operation.
	// If a parameter is already defined at the Path Item, the new definition will override it,
	// but can never remove it. The list MUST NOT include duplicated parameters.
	// A unique parameter is defined by a combination of a name and location.
	// The list can use the Reference Object to link to parameters that are defined
	// at the Swagger Object's parameters. There can be one "body" parameter at most.
	Parameters []*Parameter `json:"parameters,omitempty"`
	// Required. The list of possible responses as they are returned from executing this operation.
	// {HTTP Status Code} -> *Response. Example: "200":{...}.
	Responses map[string]*Response `json:"responses"`
	// The transfer protocol for the operation. Values MUST be from the list: "http", "https", "ws", "wss".
	// The value overrides the Swagger Object schemes definition.
	Schemes []string `json:"schemes,omitempty"`
	// Declares this operation to be deprecated.
	// Usage of the declared operation should be refrained. Default value is false.
	Deprecated bool `json:"deprecated,omitempty"`
	// A declaration of which security schemes are applied for this operation.
	// The list of values describes alternative security schemes that can be used
	// (that is, there is a logical OR between the security requirements).
	// This definition overrides any declared top-level security.
	// To remove a top-level security declaration, an empty array can be used.
	Security []string `json:"security,omitempty"`
}

// ExternalDocumentation allows referencing an external resource for extended documentation.
type ExternalDocumentation struct {
	// A short description of the target documentation. GFM syntax can be used for rich text representation.
	Description string `json:"description,omitempty"`
	// Required. The URL for the target documentation. Value MUST be in the format of a URL.
	URL string `json:"url"`
}

// ParameterIn is a location of the parameter.
type ParameterIn string

// Possible values of location of the parameter.
const (
	ParameterInQuery    ParameterIn = "query"
	ParameterInHeader   ParameterIn = "header"
	ParameterInPath     ParameterIn = "path"
	ParameterInFormData ParameterIn = "formData"
	ParameterInBody     ParameterIn = "body"
)

// Parameter describes a single operation parameter.
type Parameter struct {
	// Required. The name of the parameter. Parameter names are case sensitive.
	// If in is "path", the name field MUST correspond to the associated path segment from
	// the path field in the Paths Object. See Path Templating for further information.
	// For all other cases, the name corresponds to the parameter name used based on the in property.
	Name string `json:"name"`
	// Required. The location of the parameter.
	// Possible values are "query", "header", "path", "formData" or "body".
	In ParameterIn `json:"in"`
	// A brief description of the parameter. This could contain examples of use.
	// GFM syntax can be used for rich text representation.
	Description string `json:"description,omitempty"`
	// Determines whether this parameter is mandatory.
	// If the parameter is in "path", this property is required and its value MUST be true.
	// Otherwise, the property MAY be included and its default value is false.
	Required bool `json:"required,omitempty"`

	//
	// if "In" is "body":
	//
	// Required. The schema defining the type used for the body parameter.
	Schema *Schema `json:"schema,omitempty"`
}

// Response describes a single response from an API Operation.
type Response struct {
	// Required. A short description of the response. GFM syntax can be used for rich text representation.
	Description string `json:"description"`
	// A definition of the response structure. It can be a primitive, an array or an object.
	// If this field does not exist, it means no content is returned as part of the response.
	// As an extension to the Schema Object, its root type value may also be "file".
	// This SHOULD be accompanied by a relevant produces mime-type.
	Schema *Schema `json:"schema,omitempty"`
	// A list of headers that are sent with the response.
	Headers Headers `json:"headers,omitempty"`
	// An example of the response message.
	Examples *Example `json:"examples,omitempty"`
}

// Headers lists the headers that can be sent as part of a response.
// HeaderName -> *Head. Example: "X-Rate-Limit-Limit": {...}.
type Headers map[string]Header

// Header is a HTTP header.
// See https://swagger.io/specification/v2/#headerObject
//  {"description": "The number of ...","type": "integer"}
type Header map[string]interface{}

// Example allows sharing examples for operation responses.
// See https://swagger.io/specification/v2/#exampleObject
// mime_type -> Any. Example: "application/json": {"name": "Puma"}.
type Example map[string]interface{}

// Tag allows adding meta data to a single tag that is used by the Operation Object.
// It is not mandatory to have a Tag Object per tag used there.
type Tag struct {
	// Required. The name of the tag.
	Name string `json:"name"`
	// A short description for the tag. GFM syntax can be used for rich text representation.
	Description string `json:"description,omitempty"`
	// Additional external documentation for this tag.
	ExternalDocs *ExternalDocumentation `json:"externalDocs,omitempty"`
}

// SchemaType is a primitive data types in the Swagger Specification are based on the
// types supported by the JSON-Schema Draft 4.
// Models are described using the Schema Object which is a subset of JSON Schema Draft 4.
type SchemaType string

// List of the types.
const (
	SchemaTypeInteger SchemaType = "integer"
	SchemaTypeNumber  SchemaType = "number"
	SchemaTypeString  SchemaType = "string"
	SchemaTypeBoolean SchemaType = "boolean"
	SchemaTypeArray   SchemaType = "array"
	SchemaTypeObject  SchemaType = "object"
)

// SchemaFormat primitives have an optional modifier property format.
// Swagger uses several known formats to more finely define the data type being used.
// However, the format property is an open string-valued property,
// and can have any value to support documentation needs.
// Formats such as "email", "uuid", etc., can be used even though
// they are not defined by this specification.
type SchemaFormat string

// List of the schema formats.
const (
	// SchemaFormatInt32 + SchemaTypeInteger -> signed 32 bits.
	SchemaFormatInt32 SchemaFormat = "int32"
	// SchemaFormatInt64 + SchemaTypeInteger -> signed 64 bits.
	SchemaFormatInt64 SchemaFormat = "int64"
	// SchemaFormatFloat + SchemaTypeNumber.
	SchemaFormatFloat SchemaFormat = "float"
	// SchemaFormatDouble + SchemaTypeNumber.
	SchemaFormatDouble SchemaFormat = "double"
	// SchemaFormatByte + SchemaTypeString -> base64 encoded characters.
	SchemaFormatByte SchemaFormat = "byte"
	// SchemaFormatBinary + SchemaTypeString -> any sequence of octets.
	SchemaFormatBinary SchemaFormat = "binary"
	// SchemaFormatDate + SchemaTypeString -> as defined by full-date - RFC3339.
	SchemaFormatDate SchemaFormat = "date"
	// SchemaFormatDateTime + SchemaTypeString -> as defined by date-time - RFC3339.
	SchemaFormatDateTime SchemaFormat = "date-time"
	// SchemaFormatPassword + SchemaTypeString -> used to hint UIs the input needs to be obscured.
	SchemaFormatPassword SchemaFormat = "password"
)

// Schema allows the definition of input and output data types.
// These types can be objects, but also primitives and arrays.
// This object is based on the JSON Schema Specification Draft 4 and uses a predefined subset of it.
// On top of this subset, there are extensions provided by this specification to allow
// for more complete documentation.
type Schema struct {
	// A name of scheme MUST be unique.
	Name string `json:"-"`

	// As a JSON Reference
	Ref string `json:"$ref,omitempty"`

	// Short title of the schema.
	Title string `json:"title,omitempty"`
	// A verbose explanation of the schema.
	Description string `json:"description,omitempty"`

	// Type of the schema.
	Type SchemaType `json:"type,omitempty"`
	// Format of the type schema.
	Format SchemaFormat `json:"format,omitempty"`

	// Schema of the "array" items.
	Items *Schema `json:"items,omitempty"`
	// Enum list of string type.
	Enum []string `json:"enum,omitempty"`
	// List of the required properties.
	Required []string `json:"required,omitempty"`
	// Properties list of the properties if Type is object.
	// "propName" -> *Schema
	Properties OrderedMap `json:"properties,omitempty"`
	// Field for Map/Dictionary Properties.
	AdditionalProperties *Schema `json:"additionalProperties,omitempty"`

	// Example of the model
	Example Example `json:"example,omitempty"`
}

// Reference creates schema type with Ref field only.
// Ref field points to Definitions block of the Doc.
func (s *Schema) Reference() *Schema {
	return &Schema{
		Ref: fmt.Sprintf("#/definitions/%s", s.Name),
	}
}

// SecurityType type of the security scheme.
type SecurityType string

// Valid values of SecurityType.
const (
	SecurityTypeBasic  SecurityType = "basic"
	SecurityTypeAPIKey SecurityType = "apiKey"
	SecurityTypeOAuth2 SecurityType = "oauth2"
)

// SecurityAPIKeyIn is a location of the API key.
type SecurityAPIKeyIn string

// Valid values of SecurityAPIKeyIn.
const (
	SecurityAPIKeyInQuery  SecurityAPIKeyIn = "query"
	SecurityAPIKeyInHeader SecurityAPIKeyIn = "header"
)

// SecurityOAuth2Flow is a flow used by the OAuth2 security scheme.
type SecurityOAuth2Flow string

// Valid values of SecurityOAuth2Flow.
const (
	SecurityOAuth2FlowImplicit    SecurityOAuth2Flow = "implicit"
	SecurityOAuth2FlowPassword    SecurityOAuth2Flow = "password"
	SecurityOAuth2FlowApplication SecurityOAuth2Flow = "application"
	SecurityOAuth2FlowAccessCode  SecurityOAuth2Flow = "accessCode"
)

// Security allows the definition of a security scheme that can be used by the operations.
// Supported schemes are basic authentication, an API key
// (either as a header or as a query parameter) and
// OAuth2's common flows (implicit, password, application and access code).
type Security struct {
	// Required. The type of the security scheme. Valid values are "basic", "apiKey" or "oauth2".
	Type SecurityType `json:"type"`
	// A short description for security scheme.
	Description string `json:"description,omitempty"`

	//
	// apiKey:
	//

	// Required. The name of the header or query parameter to be used.
	Name string `json:"name,omitempty"`
	// Required The location of the API key. Valid values are "query" or "header".
	In SecurityAPIKeyIn `json:"in,omitempty"`

	//
	// oauth2:
	//

	// Required. The flow used by the OAuth2 security scheme.
	// Valid values are "implicit", "password", "application" or "accessCode".
	Flow SecurityOAuth2Flow `json:"flow,omitempty"`
	// Required. The available scopes for the OAuth2 security scheme.
	Scopes map[string]string `json:"scopes,omitempty"`

	// Required. The authorization URL to be used for this flow. This SHOULD be in the form of a URL.
	// oauth2 ("implicit", "accessCode").
	AuthorizationURL string `json:"authorizationUrl,omitempty"`
	// Required. The token URL to be used for this flow. This SHOULD be in the form of a URL.
	// oauth2 ("password", "application", "accessCode")
	TokenURL string `json:"tokenUrl,omitempty"`
}
